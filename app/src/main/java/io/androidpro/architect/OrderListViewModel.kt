package io.androidpro.architect

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.androidpro.architect.OrderListState.Data
import io.androidpro.domain.OrdersRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class OrderListViewModel(
    private val repo: OrdersRepository,
    private val mapper: OrderListItemDisplayableMapper
) : ViewModel() {

    private val _state = MutableStateFlow<OrderListState>(OrderListState.Loading)
    val state: StateFlow<OrderListState>
        get() = _state.asStateFlow()

    init {
        loadData()
    }

    private fun loadData() {
        viewModelScope.launch {
            val items = repo.getAll().map(mapper::map)
            _state.value = items.let(::Data)
        }
    }

}

sealed interface OrderListState {
    object Loading : OrderListState
    object Error : OrderListState
    data class Data(val orders: List<OrderListItemDisplayable>) : OrderListState
}

