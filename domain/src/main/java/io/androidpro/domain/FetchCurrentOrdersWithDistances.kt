package io.androidpro.domain

class FetchCurrentOrdersWithDistances(
    private val ordersRepository: OrdersRepository,
    private val gpsDataProvider: GpsDataProvider
){
    suspend fun execute(): List<OrderWithDistance>{
        // calculate distance to pickup or delivery
        // use Haversine formula
        // https://en.wikipedia.org/wiki/Haversine_formula
        // https://www.rosettacode.org/wiki/Haversine_formula#Kotlin
        return emptyList()
    }
}

data class OrderWithDistance(
    val order: Order,
    val distance: Distance
)

@JvmInline
value class Distance(val value: Int)